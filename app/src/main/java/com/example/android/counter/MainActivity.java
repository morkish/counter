package com.example.android.counter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Integer count = 0;
    boolean truefalse = false;
    private String m_Text = "";
    private static final int SECOND_ACTIVITY_RESULT_CODE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void increase(View view){
        count = count +1;
        updateCounter(count);
    }
    public void decrease(View view){
        count = count -1;
        updateCounter(count);
    }
    public void zero(View view){
        count = 0;
        updateCounter(count);
    }
    public void updateCounter(Integer count) {
        TextView counterText = (TextView)findViewById(R.id.counter);
        counterText.setText(count.toString());
    }
    public void update(View view){
        TextView editUpdate = (TextView) findViewById(R.id.editUpdate);
        String numberToUpdate = editUpdate.getText().toString();
        TextView counter = (TextView)findViewById(R.id.counter);
        counter.setText(numberToUpdate);
        count = Integer.parseInt(numberToUpdate);
    }
    public void insertViaAlert(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Please insert your value");

// Set up the input
        final EditText input = new EditText(this);
// Specify the type of input expected;
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

// Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                m_Text = input.getText().toString();
                TextView counterText = (TextView)findViewById(R.id.counter);
                counterText.setText(m_Text);
                count = Integer.parseInt(m_Text);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }
    // "Go to Second Activity" button click
    public void insertViaIntent(View view){
        // Start the SecondActivity
        Intent intent = new Intent(this, Main2Activity.class);
        startActivityForResult(intent, SECOND_ACTIVITY_RESULT_CODE);
    }

    // This method is called when the second activity finishes
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // check that it is the SecondActivity with an OK result
        if (requestCode == SECOND_ACTIVITY_RESULT_CODE) {
            if (resultCode == RESULT_OK) {

                // get String data from Intent
                String returnString = data.getStringExtra("keyName");
                count = Integer.parseInt(returnString);
                // set text view with string
                TextView textView = (TextView) findViewById(R.id.counter);
                textView.setText(returnString);
            }
        }
    }

    public void showEdit(View view){
        truefalse = !truefalse;
        if (truefalse) {
            TextView editUpdate = (TextView) findViewById(R.id.editUpdate);
            editUpdate.setVisibility(View.VISIBLE);
            TextView buttonUpdate = (Button) findViewById(R.id.buttonUpdate);
            buttonUpdate.setVisibility(View.VISIBLE);
        }
        else{
            TextView editUpdate = (TextView) findViewById(R.id.editUpdate);
            editUpdate.setVisibility(View.INVISIBLE);
            TextView buttonUpdate = (Button) findViewById(R.id.buttonUpdate);
            buttonUpdate.setVisibility(View.INVISIBLE);
        }
    }
}
